Source: fonts-sil-mondulkiri-extra
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Hideki Yamane <henrich@debian.org>,
Build-Depends: debhelper-compat (= 13),
Standards-Version: 4.5.0
Homepage: https://software.sil.org/mondulkiri/
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-mondulkiri-extra.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-mondulkiri-extra
Rules-Requires-Root: no

Package: fonts-sil-mondulkiri-extra
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Mondulkiri OpenType font family for Khmer script - additional fonts
 The Mondulkiri fonts provide Unicode support for the Khmer script. Mondulkiri
 is the name of a province in north-eastern Cambodia, Busra and Oureang are
 names of places in that province. Ratanakiri is the name of another province in
 north-eastern Cambodia.
 .
 The following fonts from the Mondulkiri typeface family are included in
 this release:
 .
     * Khmer Busra Bunong Regular
     * Khmer Busra diagnostic Regular
     * Khmer Busra dict Regular
     * Khmer Busra dot Regular
     * Khmer Busra high Regular
     * Khmer Busra MOE Regular
     * Khmer Busra xspace Regular
     * Khmer Oureang Ultra-expanded UltraBlack - this is an extra bold font
         useful for headings
     * Khmer Ratanakiri Regular - this is a Mool (or Muol or Muul) font which is
         frequently used for headings and signs.
